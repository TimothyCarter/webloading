﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using WebLoading.iOS;

using UIKit;

[assembly:ExportRenderer(typeof(WebView),typeof(WebViewCustomRenderer))]

namespace WebLoading.iOS
{
	public class WebViewCustomRenderer : WebViewRenderer
	{
		Xamarin.Forms.WebView formsWebView;

		protected override void OnElementChanged (VisualElementChangedEventArgs e)
		{
			base.OnElementChanged (e);

			if (e.NewElement != null) {
				formsWebView = e.NewElement as WebView;

				//Pass existing delegate
				var existingDelegate = Delegate;
				Delegate = new CustomWebViewDelegate (formsWebView, existingDelegate); // Pass a reference to what was already being used
			}
		}

		public class CustomWebViewDelegate : UIWebViewDelegate
		{
			UIKit.IUIWebViewDelegate existingDelegate;
			Xamarin.Forms.WebView formsWebView;
			public event EventHandler<WebNavigatedEventArgs> Navigated;
			public event EventHandler<WebNavigatingEventArgs> Navigating;
			WebNavigationEvent lastEvent;

			public CustomWebViewDelegate(Xamarin.Forms.WebView webView, UIKit.IUIWebViewDelegate theExistingDelegate)
			{
				existingDelegate = theExistingDelegate;
				formsWebView = webView;
				Navigated += App.WebViewNavigatedEventHandler;
				Navigating += App.WebViewNavigatingEventHandler;
			}

			public override void LoadingFinished (UIWebView webView)
			{
				existingDelegate.LoadingFinished (webView);
				var url = webView.Request.Url.AbsoluteUrl.ToString ();
				var args = new WebNavigatedEventArgs (lastEvent, formsWebView.Source, url, WebNavigationResult.Success);

				Navigated (formsWebView, args);
			}

			public override bool ShouldStartLoad (UIWebView webView, Foundation.NSUrlRequest request, UIWebViewNavigationType navigationType)
			{
				existingDelegate.ShouldStartLoad (webView, request, navigationType);
				var url = request.Url.AbsoluteUrl.ToString ();
				var args = new WebNavigatingEventArgs (WebNavigationEvent.NewPage, formsWebView.Source, url);

				webView.ScalesPageToFit = true;

				WebNavigationEvent navEvent = WebNavigationEvent.NewPage;
				switch (navigationType) {
				case UIWebViewNavigationType.LinkClicked:
					navEvent = WebNavigationEvent.NewPage;
					break;
				case UIWebViewNavigationType.FormSubmitted:
					navEvent = WebNavigationEvent.NewPage;
					break;
				case UIWebViewNavigationType.Reload:
					navEvent = WebNavigationEvent.Refresh;
					break;
				case UIWebViewNavigationType.FormResubmitted:
					navEvent = WebNavigationEvent.NewPage;
					break;
				case UIWebViewNavigationType.Other:
					navEvent = WebNavigationEvent.NewPage;
					break;
				}

				lastEvent = navEvent;

				Console.WriteLine ("[Custom Delegate] Url: {0}", request.Url);
				Console.WriteLine ("[Custom Delegate] MainDocUrl: {0}", request.MainDocumentURL);

				Navigating (formsWebView, args);
				return true;
			}
		}
	}
}

