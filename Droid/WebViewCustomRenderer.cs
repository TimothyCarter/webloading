﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using WebLoading.Droid;

using Android.Webkit;

[assembly:ExportRenderer(typeof(Xamarin.Forms.WebView),typeof(WebViewCustomRenderer))]

namespace WebLoading.Droid
{
	public class WebViewCustomRenderer : WebViewRenderer
	{
		Xamarin.Forms.WebView formsWebView;

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.WebView> e)
		{
			base.OnElementChanged (e);

			if (e.NewElement != null) {
				formsWebView = e.NewElement;

				//Get the existing web client
				var existingClient = new WebViewClient();
				Control.SetWebViewClient (new CustomWebViewClient(formsWebView,existingClient));
			}
		}
	}

	public class CustomWebViewClient : WebViewClient
	{
		WebViewClient existingClient;
		Xamarin.Forms.WebView formsWebView;
		public event EventHandler<WebNavigatedEventArgs> Navigated;
		public event EventHandler<WebNavigatingEventArgs> Navigating;

		public CustomWebViewClient(Xamarin.Forms.WebView webView,WebViewClient theExistingClient)
		{
			existingClient = theExistingClient;
			formsWebView = webView;
			Navigated += App.WebViewNavigatedEventHandler;
			Navigating += App.WebViewNavigatingEventHandler;
		}

		public override WebResourceResponse ShouldInterceptRequest (Android.Webkit.WebView view, IWebResourceRequest request)
		{
			Console.WriteLine ("[Custom Delegate] Url: {0}", request.Url);

			return base.ShouldInterceptRequest (view, request);
		}

		public override bool ShouldOverrideUrlLoading (Android.Webkit.WebView view, string url)
		{
			view.Settings.JavaScriptEnabled = true;
			view.Settings.JavaScriptCanOpenWindowsAutomatically = true;

			view.LoadUrl (url);
			return true;
		}

		public override void OnPageStarted (Android.Webkit.WebView view, string url,Android.Graphics.Bitmap favicon)
		{
			existingClient.OnPageStarted (view,url,favicon);
			var source = new UrlWebViewSource{ Url = url };
			var args = new WebNavigatingEventArgs (WebNavigationEvent.NewPage, source, url);

			Navigating (formsWebView, args);
			base.OnPageStarted (view, url,favicon);
		}

		public override void OnPageFinished (Android.Webkit.WebView view, string url)
		{
			existingClient.OnPageFinished (view, url);
			var source = new UrlWebViewSource{ Url = url };
			var args = new WebNavigatedEventArgs (WebNavigationEvent.NewPage, source, url, WebNavigationResult.Success);

			Navigated (formsWebView, args);
			base.OnPageFinished (view, url);
		}
	}
}