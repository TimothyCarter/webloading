﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Diagnostics;

namespace WebLoading
{
	public class isNavigatingModel : INotifyPropertyChanged
	{
		bool _isNavigating;

		public event PropertyChangedEventHandler PropertyChanged;

		public isNavigatingModel ()
		{
			_isNavigating = false;
		}

		public bool isNavigating
		{
			set 
			{
				Debug.WriteLine ("Set is Navigating to: " + value.ToString());

				if (_isNavigating != value) 
				{
					_isNavigating = value;

					if (PropertyChanged != null) 
					{
						PropertyChanged (this, new PropertyChangedEventArgs ("isNavigating"));
					}
				}
			}
			get 
			{
				return _isNavigating;
			}
		}
	}
}

