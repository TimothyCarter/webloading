﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace WebLoading
{
	public partial class App : Application
	{
		public static isNavigatingModel currentNavigatingModel { get; set; }

		public App ()
		{
			InitializeComponent();

			currentNavigatingModel = new isNavigatingModel ();

			NavigationPage mainNavPage = new NavigationPage (new webPage("https://www.xamarin.com/"));

			MainPage = mainNavPage;
		}

		public static void WebViewNavigatingEventHandler(object sender, WebNavigatingEventArgs e)
		{
			currentNavigatingModel.isNavigating = true;

			Debug.WriteLine("[Forms WebView Navigating] {0}", e.Url.ToString());
		}

		public static void WebViewNavigatedEventHandler(object sender, WebNavigatedEventArgs e)
		{
			currentNavigatingModel.isNavigating = false;

			Debug.WriteLine("[Forms WebView Navigated] {0}", e.Url.ToString());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

