﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using System.Threading.Tasks;

namespace WebLoading
{
	public partial class webPage : ContentPage
	{
		UrlWebViewSource currentSource = new UrlWebViewSource ();

		public webPage (string url)
		{
			InitializeComponent (); 

			this.BindingContext = App.currentNavigatingModel;

			currentSource.Url = url;
		
			currentWebView.Source = currentSource;
		}
	}
		
}

